/**
 * nodejs-restapi
 * Created by andreykubasov on 03.08.17.
 */
let express = require('express');
let router = express.Router();
let jwt = require('jsonwebtoken');
let Device = require('../models/devices');

const jwt_secret = "kubas";

router.post('/add', function(req, res){
    Device.count({},(err,count)=>{
        let device = new Device({
            id: count+1,
            name: req.body.name,
            type: req.body.type,
            activated: req.body.activated
        });
        device.save(function(err, data){
            if(err){
                return res.json({error: true});
            }
            res.json({error:false});
        })
    } );
});

router.get('/', function(req, res){
    Device.find().lean().exec(function(err, devices){
        if(err){
            return res.json({error: true});
        }
        if(!devices){
            return res.status(404).json({'message':'devices not found!'});
        }
        res.json(devices);
    })
});

module.exports = router;