/**
 * nodejs-restapi
 * Created by andreykubasov on 03.08.17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = mongoose.model('Device', new Schema({
    id: Number,
    name: String,
    type: String,
    activated: Boolean
}));
